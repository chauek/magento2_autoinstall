Magento 2 auto installer
=======================

This script uses https://github.com/alankent/vagrant-magento2-apache-base repo to install Magento 2 project.
There are some extra tweaks like:

* added phpmyadmin
* changed apache2 user to vagrant so it can write to var in shared source
* added composer install
* added installation of Magento 2
