#!/bin/bash

if [[ "$@" == "-h" ]]
then
    cat << EOF
Usage: install_magento2.sh [OPTION]

Options:
  -r    Resets prevoius installation.
EOF
    exit 1;
fi

if [[ "$@" == "-r" ]]
then
    DO_RESET=1
else
    DO_RESET=0
fi


if [ -d vagrant-magento2-apache-base ]
then
    if [ "DO_RESET" == "1" ]
    then
        cd vagrant-magento2-apache-base
        echo "Destrying existing vagrant..."
        vagrant destroy -f
        cd ..
        rm -rf ./vagrant-magento2-apache-base
    else
        echo "There ids already vagrant dir. To reinstall use -r option"
        exit 1;
    fi
fi

if [ -d magento2 ]
then
    if [ "DO_RESET" == "1" ]
    then
        rm -rf ./magento2
    else
        echo "There ids already Magento 2 dir. To reinstall use -r option"
        exit 1;
    fi
fi

git clone https://github.com/magento/magento2.git
git clone https://github.com/alankent/vagrant-magento2-apache-base.git

cd vagrant-magento2-apache-base
vagrant up

vagrant ssh -- "sudo sed -i -e 's/export APACHE_RUN_USER=www-data/export APACHE_RUN_USER=vagrant/g' /etc/apache2/envvars"
vagrant ssh -- "sudo sed -i -e 's/export APACHE_RUN_GROUP=www-data/export APACHE_RUN_GROUP=vagrant/g' /etc/apache2/envvars"
vagrant ssh -- "sudo service apache2 restart"

MYADMIN_CONF="sudo debconf-set-selections <<< 'phpmyadmin phpmyadmin/dbconfig-install boolean true'"
MYADMIN_CONF="$MYADMIN_CONF; sudo debconf-set-selections <<< 'phpmyadmin phpmyadmin/app-password-confirm password secret'"
MYADMIN_CONF="$MYADMIN_CONF; sudo debconf-set-selections <<< 'phpmyadmin phpmyadmin/mysql/admin-pass password secret'"
MYADMIN_CONF="$MYADMIN_CONF; sudo debconf-set-selections <<< 'phpmyadmin phpmyadmin/mysql/app-pass password secret'"
MYADMIN_CONF="$MYADMIN_CONF; sudo debconf-set-selections <<< 'phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2'"
vagrant ssh -- "$MYADMIN_CONF; sudo apt-get -y install phpmyadmin"

vagrant ssh -- "cd /var/www/magento2 && COMPOSER_PROCESS_TIMEOUT=2000 composer install -o -v"
vagrant ssh -- "sh /vagrant/scripts/deploy.sh"

echo "====================================================================="
echo " You can go to:"
echo "  * http://127.0.0.1:8080/ to see M2 front"
echo "  * http://127.0.0.1:8080/admin (admin:admin123) for admin panel"
echo "  * http://127.0.0.1:8080/phpmyadmin/ (root:sercet) for phpmyadmin"
echo "====================================================================="
echo 



